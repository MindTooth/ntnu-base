FROM gossin/iikos-ci

# Install packages
RUN apt-get update && apt-get install -y \
	build-essential \
	clang-format-9 \
	clang-tidy \
	curl \
	git \
	python3-pip \
	shellcheck \
	valgrind \
	&& rm -rf /var/lib/apt/lists/*

# Link the newer clang-format
RUN ln -s /usr/bin/clang-format-9 /usr/bin/clang-format

# Install shfmt
RUN curl -s -L -o shfmt https://github.com/mvdan/sh/releases/download/v3.0.2/shfmt_v3.0.2_linux_amd64 && \
	chmod 755 shfmt && mv shfmt /usr/local/bin

# Install pre-commit
RUN pip3 install pre-commit
